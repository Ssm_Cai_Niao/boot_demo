const key = '0123456789abcdef';
const iv = '0123456789abcdef';

function myEncrypt(data) {
    const isValid = getCookie("isValid");
    if(isValid === "false"){
        return data;
    }
    let encryptKey = CryptoJS.enc.Utf8.parse(key);
    let encryptIv = CryptoJS.enc.Utf8.parse(iv);
    const content = JSON.stringify(data);
    let encrypted = CryptoJS.AES.encrypt(content, encryptKey,{
        iv:encryptIv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });
    return encrypted.ciphertext.toString();
}

function getCookie(cookieName) {
    var cookies = document.cookie.split('; ');
    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i].split('=');
        if (cookie[0] === cookieName) {
            return cookie[1];
        }
    }
    return false;
}


