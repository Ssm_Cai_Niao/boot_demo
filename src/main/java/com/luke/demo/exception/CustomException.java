package com.luke.demo.exception;

import com.luke.demo.config.Result;

/**
 * @author luke
 * @date 2024-06-08 22:39:37
 **/
public class CustomException extends RuntimeException{

    private Result result;

    public CustomException(String message){
        super(message);
        Result result = new Result();
        result.setMsg(message);
        result.setCode(500);
    }

    public Result getResult() {
        return result;
    }
}
