package com.luke.demo.service;

import com.luke.demo.datasource.mysql.MyUserDataSource;
import com.luke.demo.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * @author luke
 * @date 2024-05-31 21:26:43
 **/
@Service
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    MyUserDataSource myUserDataSource;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Employee employee = myUserDataSource.get(username);
        if(employee == null){
            throw new UsernameNotFoundException("用户不存在");
        }
        return new User(username, employee.getPassword(), new ArrayList<>());
    }


}
