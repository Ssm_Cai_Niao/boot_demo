package com.luke.demo.filter;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

/**
 * @author luke
 * @date 2024-06-01 13:04:15
 * 过滤器
 **/
@Component
public class RequestFilter extends OncePerRequestFilter {

    

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String url = request.getRequestURI();
        String queryString = request.getQueryString();

        System.out.println("请求地址: " + url);
        if (queryString != null) {
            System.out.println("请求参数: " + queryString);
        }

        Enumeration<String> parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String paramName = parameterNames.nextElement();
            String paramValue = request.getParameter(paramName);
            System.out.println("参数: " + paramName + " = " + paramValue);
        }

        filterChain.doFilter(request, response);
    }


}
