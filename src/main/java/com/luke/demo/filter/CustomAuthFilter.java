package com.luke.demo.filter;

import com.luke.demo.config.MyPropertiesConfig;
import com.luke.demo.datasource.mysql.MyUserDataSource;
import com.luke.demo.datasource.redis.MyRedisTemplate;
import com.luke.demo.entity.Employee;
import com.luke.demo.utils.RsaUtils;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * @author luke
 * @date 2024-06-08 23:45:21
 **/
@Component
public class CustomAuthFilter implements AuthenticationProvider{

    @Autowired
    private MyUserDataSource myUserDataSource;
    @Autowired
    private MyRedisTemplate myRedisTemplate;
    @Autowired
    MyPropertiesConfig myPropertiesConfig;


    @SneakyThrows
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        final String username = String.valueOf(authentication.getPrincipal());
        final String password = String.valueOf(authentication.getCredentials());
        Employee employee = (Employee)myRedisTemplate.opsForValue().get("user");
        if(employee == null){
            employee = myUserDataSource.get(username);
            if(employee == null){
                throw new UsernameNotFoundException("用户不存在");
            }
        }
        if (myPropertiesConfig.isEnabled()) {
            if(!employee.getPassword().equals(RsaUtils.decrypt(password))){
                throw new UsernameNotFoundException("密码错误");
            }
        }else{
            if(!employee.getPassword().equals(password)){
                throw new UsernameNotFoundException("密码错误");
            }
        }
        myRedisTemplate.opsForValue().set("user",employee);
        return new UsernamePasswordAuthenticationToken(employee, password, new ArrayList<>());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
}
