package com.luke.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author luke
 * @date 2024-06-09 00:45:58
 **/
@Component
@ConfigurationProperties(prefix = "custom.valid")
public class MyPropertiesConfig {

    private boolean enabled = true;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
