package com.luke.demo.config;

import lombok.Data;

/**
 * @author luke
 * @date 2024-06-01 02:02:07
 **/
@Data
public class Result<T> {

    private int code;

    private String msg;

    private T data;

    public static <T> Result<T> success(String msg){
        Result<T> result = new Result<>();
        result.setCode(0);
        result.setMsg(msg);
        return result;
    }

    public static <T> Result<T> success(T data){
        Result<T> result = new Result<>();
        result.setData(data);
        return result;
    }

    public static <T> Result<T> success(String msg,T data){
        Result<T> result = new Result<>();
        result.setMsg(msg);
        result.setData(data);
        return result;
    }

    public static <T> Result<T> error(String msg) {
        Result<T> result = new Result<>();
        result.setCode(500);
        result.setMsg(msg);
        return result;
    }

    public static <T> Result<T> error(int code, String msg) {
        Result<T> result = new Result<>();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    public static <T> Result<T> saveResult(boolean isFlag){
        return isFlag ? Result.success("添加成功") : Result.error("添加失败");
    }
}
