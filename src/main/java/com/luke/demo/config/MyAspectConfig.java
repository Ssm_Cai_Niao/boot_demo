package com.luke.demo.config;

import com.alibaba.fastjson.JSONObject;
import com.luke.demo.utils.AesUtils;
import com.luke.demo.utils.RsaUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Map;

/**
 * @author luke
 * @date 2024-06-09 01:24:10
 **/
@Component
@Aspect
public class MyAspectConfig {

    @Autowired
    private MyPropertiesConfig myPropertiesConfig;

    @Before("@annotation(com.luke.demo.annotation.CustomAnnotation)")
    public void beforeRegister(JoinPoint joinPoint) throws Exception {
        if (!myPropertiesConfig.isEnabled()) {
            return;
        }
        for (Object arg : joinPoint.getArgs()) {
            Class<?> argClass = arg.getClass();
            Field dataField = argClass.getSuperclass().getDeclaredField("data");
            dataField.setAccessible(true);
            String data = (String) dataField.get(arg);
            JSONObject jsonObject = JSONObject.parseObject(AesUtils.decrypt(data));
            Map<String, Object> map = jsonObject.getInnerMap();

            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                if(key.equals("password")){
                    value = RsaUtils.decrypt((String)value);
                }
                Field field = argClass.getDeclaredField(key);
                field.setAccessible(true);
                field.set(arg, value);
            }
        }
    }

}
