package com.luke.demo.config;


import com.luke.demo.datasource.mysql.MyUserDataSource;
import com.luke.demo.filter.CustomAuthFilter;
import com.luke.demo.filter.RequestFilter;
import com.luke.demo.handler.LoginFailureHandler;
import com.luke.demo.handler.LoginSuccessHandler;
import com.luke.demo.service.CustomUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * @author luke
 * @date 2024-05-31 21:38:03
 **/
@EnableWebSecurity
@Configuration
public class MySecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    LoginSuccessHandler loginSuccessHandler;

    @Autowired
    LoginFailureHandler loginFailureHandler;

    @Autowired
    MyUserDataSource myUserDataSource;

    @Autowired
    CustomAuthFilter customAuthFilter;

    @Override
    public void configure(WebSecurity web) {
        //解决静态资源被拦截的问题
        web.ignoring().antMatchers("/js/**");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth){
        auth.authenticationProvider(customAuthFilter);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests().antMatchers("/a/register","/a/login","/a/view/**").permitAll().anyRequest().authenticated().and()
                .formLogin()
                .successHandler(loginSuccessHandler)
                .failureHandler(loginFailureHandler)
                .loginPage("/a/main")   //自定义登录页面
                // loginProcessingUrl 配置了无法使用问题 https://www.cnblogs.com/cokes/p/16287933.html
                .loginProcessingUrl("/a/login") // 登录访问路径
                .permitAll() // 登录成功后默认页面
                .and().csrf().disable();
    }


}
