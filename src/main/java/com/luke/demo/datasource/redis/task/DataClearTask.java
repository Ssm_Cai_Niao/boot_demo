package com.luke.demo.datasource.redis.task;

import com.luke.demo.datasource.redis.MyRedisTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author luke
 * @date 2024-06-01 19:06:39
 **/
@Component
public class DataClearTask {


    @Autowired
    private MyRedisTemplate myRedisTemplate;

    @Scheduled(fixedRate = 1000) // 每秒执行一次
    public void cleanupExpiredData() {
        Map<String, MyRedisTemplate> map = myRedisTemplate.getMap();
        long currentTime = System.currentTimeMillis() / 1000; // 当前时间（秒）
        for (String key : map.keySet()) {
            MyRedisTemplate template = myRedisTemplate.getMap().get(key);
            if (template != null && template.getTime() != null) {
                if ((currentTime - template.getTime()) >= 1) { // 过期时间为1秒
                    myRedisTemplate.getMap().remove(key);
                    System.out.println("已清除过期数据：" + key);
                }
            }
        }
    }

}
