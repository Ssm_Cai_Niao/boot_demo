package com.luke.demo.datasource.redis;

import lombok.Data;
import lombok.Getter;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author luke
 * @date 2024-06-01 18:52:58
 **/
@Component
public class MyRedisTemplate {

    private String key;
    private Object data;
    private Long time;
    private Map<String, MyRedisTemplate> map = new HashMap<>();

    public OpsForValue opsForValue() {
        return new OpsForValue();
    }

    public class OpsForValue {

        /**
         * 设置键值对和过期时间
         *
         * @param key   键
         * @param data  存储数据
         * @param time  过期时间（毫秒）
         */
        public void set(String key, Object data, long time) {
            MyRedisTemplate.this.key = key;
            MyRedisTemplate.this.data = data;
            MyRedisTemplate.this.time = time;
            MyRedisTemplate.this.map.put(key, MyRedisTemplate.this);
        }

        /**
         * 设置键值对，默认过期时间为30天
         * @param key   键
         * @param data  存储数据
         */
        public void set(String key, Object data) {
            MyRedisTemplate.this.key = key;
            MyRedisTemplate.this.data = data;
            MyRedisTemplate.this.time = 30 * 24 * 60 * 60 * 1000L; // 默认过期时间为30天
            MyRedisTemplate.this.map.put(key, MyRedisTemplate.this);
        }

        public Object get(String key) {
            MyRedisTemplate myRedisTemplate = MyRedisTemplate.this.map.get(key);
            return myRedisTemplate != null ? myRedisTemplate.data : null;
        }
    }



    public Map<String, MyRedisTemplate> getMap() {
        return map;
    }

    public Long getTime() {
        return time;
    }
}
