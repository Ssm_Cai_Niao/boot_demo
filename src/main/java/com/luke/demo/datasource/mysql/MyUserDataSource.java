package com.luke.demo.datasource.mysql;

import com.luke.demo.entity.Employee;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author luke
 * @date 2024-05-31 21:23:23
 **/
@Getter
@Component
public class MyUserDataSource {

    private Map<String, Employee> userMap = new HashMap<>();

    public Employee get(String userName){
        return userMap.get(userName);
    }

    public boolean save(Employee employee) {
        Employee u = userMap.get(employee.getUsername());
        userMap.put(employee.getUsername(), employee);
        if(u != null){
            return false;
        }
        return true;
    }

    public List<Employee> findList(){
        return new ArrayList<>(userMap.values());
    }
}
