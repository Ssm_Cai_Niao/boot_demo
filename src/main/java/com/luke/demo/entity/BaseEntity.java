package com.luke.demo.entity;

import lombok.Data;

/**
 * @author luke
 * @date 2024-06-09 01:13:35
 **/
@Data
public class BaseEntity {

    private String data;

}
