package com.luke.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


/**
 * @author luke
 * @date 2024-05-31 21:11:17
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Employee extends BaseEntity{

    private String id;

    private String username;

    private String password;

    private String role;

}
