package com.luke.demo.handler;

import com.alibaba.fastjson.JSON;
import com.luke.demo.config.Result;
import com.luke.demo.datasource.redis.MyRedisTemplate;
import com.luke.demo.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author luke
 * @date 2024-06-01 02:01:18
 **/
@Component
public class LoginSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        //直接跳转界面
        //response.sendRedirect("/a/list");
        //返回json格式
        response.setContentType("application/json;charset=UTF-8");
        ServletOutputStream outputStream = response.getOutputStream();
        //生成jwt
        String token = JwtUtils.generateToken(authentication.getName());
        response.setHeader(JwtUtils.COOKIE_NAME,token);
        Result<String> result = Result.success("登陆成功");
        outputStream.write(JSON.toJSONString(result).getBytes(StandardCharsets.UTF_8));
        outputStream.flush();
        outputStream.close();

    }
}
