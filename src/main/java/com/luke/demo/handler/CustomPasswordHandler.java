package com.luke.demo.handler;

import com.luke.demo.utils.RsaUtils;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author luke
 * @date 2024-06-08 23:37:03
 **/
public class CustomPasswordHandler implements PasswordEncoder {

    @Override
    public String encode(CharSequence rawPassword) {
        String password = rawPassword.toString();
        return null;
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return true;
    }
}
