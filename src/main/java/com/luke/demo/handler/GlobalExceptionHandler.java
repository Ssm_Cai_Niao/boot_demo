package com.luke.demo.handler;

import com.luke.demo.config.Result;
import com.luke.demo.exception.CustomException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author luke
 * @date 2024-06-08 22:43:39
 **/
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(CustomException.class)
    public Result handleCustomException(CustomException ex) {
        Result result = ex.getResult();
        return Result.error(result.getMsg());
    }

}
