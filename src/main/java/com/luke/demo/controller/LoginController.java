package com.luke.demo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.luke.demo.annotation.CustomAnnotation;
import com.luke.demo.config.MyPropertiesConfig;
import com.luke.demo.datasource.mysql.MyUserDataSource;
import com.luke.demo.config.Result;
import com.luke.demo.entity.Employee;
import com.luke.demo.utils.AesUtils;
import com.luke.demo.utils.RsaUtils;
import org.apache.commons.codec.DecoderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;

/**
 * @author luke
 * @date 2024-05-31 21:42:34
 **/
@Controller
@RequestMapping("/a")
public class LoginController {

    @Autowired
    MyUserDataSource myUserDataSource;
    @Autowired
    MyPropertiesConfig myPropertiesConfig;

    @RequestMapping("main")
    public String main(Model model, HttpServletResponse response){
        model.addAttribute("publicKey",RsaUtils.publicKeyStr);
        Cookie cookie = new Cookie("isValid", String.valueOf(myPropertiesConfig.isEnabled()));
        cookie.setMaxAge(-1); // 永久有效
        cookie.setPath("/"); // 设置 Cookie 的路径
        response.addCookie(cookie);
        return "main";
    }


    @RequestMapping("list")
    public String list(){
        return "list";
    }

    @RequestMapping("listData")
    @ResponseBody
    public Result<List<Employee>> listData(){
        List<Employee> list = myUserDataSource.findList();
        return Result.success(list);
    }

    @RequestMapping("/view/register")
    public String viewRegister(Model model)
    {
        model.addAttribute("publicKey",RsaUtils.publicKeyStr);
        return "register";
    }

    @CustomAnnotation
    @RequestMapping("register")
    @ResponseBody
    public Result<Void> register(Employee employee)  {
        if(!StringUtils.hasText(employee.getUsername()) || !StringUtils.hasText(employee.getPassword())){
            return Result.error("用户名或密码不能为空");
        }
        employee.setId(UUID.randomUUID().toString());
        employee.setRole("admin");
        return Result.saveResult(myUserDataSource.save(employee));
    }
}
